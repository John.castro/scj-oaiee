# Autor(es): John David Castro

# Fecha creación: 29/11/2021
# Versión de R: 4.1.1
#========================================================================
rm(list=ls())
#cambiar directorio de donde quiera guardar o remover la info
# setwd('C:/Users/john.castro/Desktop/ALL/mensual_comentarios')
setwd("/Users/johndavid/Downloads/boletin")
Mes<-12 #imputar mes del reporte 
descargar=F
if(descargar==T){
#Librerias
library(lubridate)
library(ROracle)
library(data.table)

# 1. descarga de delitos  ####
drv <- dbDriver("Oracle")
host <- "172.21.17.46"
port <- 1521
svc <- "pdb00001.lanbdproduccion.vcnlanglobalscj.oraclevcn.com"
connect.string <- paste(
  "(DESCRIPTION=",
  "(ADDRESS=(PROTOCOL=tcp)(HOST=", host, ")(PORT=", port, "))",
  "(CONNECT_DATA=(SERVICE_NAME=", svc, ")))", sep = "")
con <- dbConnect(drv, username = "ADMCON", password = "AdmConDWH17.",
                 dbname = connect.string)

anualVector<-("('2019','2020','2021')") #cambiar cada año en enero


################################################################################
#################### procesamiento automático ##################################
################################################################################
# descarga de datos
  siedco <- dbGetQuery(con, paste("select * from ADMDWH.TVW_SIEDCO_BOLETIN where TIPO_HECHO = 'DELITO' and ANIO in", anualVector," "))
saveRDS(siedco, paste0("input/siedco_",Mes,".RDS"))
}else{
  siedco<-readRDS(paste0("input/siedco_",Mes,".RDS"))
}
# Parametros
  library(pacman)
  p_load(dplyr, data.table, Hmisc, openxlsx)
  DIA_SEMANA <- 1:7
  day <- c("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo")
  range_day <- c("Lun-Jue", "Lun-Jue", "Lun-Jue", "Lun-Jue", "Vie-Dom", "Vie-Dom", "Vie-Dom")
  temp <- data.frame(DIA_SEMANA, day, range_day)
  df_all <- merge(siedco, temp, all.x = TRUE)
  hechos<-c("HOMICIDIOS", "LESIONES PERSONALES","HURTO A PERSONAS", "HURTO A COMERCIO",
            "DELITOS SEXUALES", "VIOLENCIA INTRAFAMILIAR", "HURTO MOTOCICLETAS",
            "HURTO AUTOMOTORES", "HURTO A RESIDENCIAS", "HURTO DE CELULARES",
            "HURTO DE BICICLETAS")
  df_all$LOCALIDAD[df_all$LOCALIDAD=="ANTONIO NARI?O"]<-"ANTONIO NARIÑO"
  
  locali1=c("USAQUEN", "CHAPINERO", "SUBA", "BARRIOS UNIDOS","TEUSAQUILLO" ) #cosec1
  locali2=c("CIUDAD BOLIVAR", "RAFAEL URIBE URIBE", "SAN CRISTOBAL", "USME" ,"TUNJUELITO","SUMAPAZ") #cosec2
  locali3=c("BOSA", "FONTIBON", "KENNEDY", "ENGATIVA" ) #cosec3
  locali4=c("SANTA FE", "CANDELARIA", "PUENTE ARANDA", "LOS MARTIRES","ANTONIO NARIÑO" ) #cosec4
  
  
  df_all<-df_all %>% mutate(cosec=case_when(LOCALIDAD %in% locali1 ~ "COSEC1",
                                            LOCALIDAD %in% locali2 ~ "COSEC2",
                                            LOCALIDAD %in% locali3 ~ "COSEC3",
                                            LOCALIDAD %in% locali4 ~ "COSEC4",
                                            TRUE ~ NA_character_))
  # df_all<-df_all %>%
  #   mutate(CARACTERISTICA=gsub("RI\\?A", "RIÑA", CARACTERISTICA),
  #     modalidad=case_when(CARACTERISTICA=="ATRACO" ~ "Atraco",
  #                         grepl(".*RIÑA*.", CARACTERISTICA) ~ "Riñas",
  #                                           CARACTERISTICA=="SICARIATO" ~ "Sicariato",
  #                                           CARACTERISTICA=="POR ESTABLECER" ~ "Por establecer",
  #                                           TRUE ~ "Otro"))  
  # 
  
  
#tabla 1 variaciones ordenadas año corrido
  
  tabla1<-df_all %>% filter(MES<=Mes, HECHO %in% hechos) %>% 
    mutate(HECHO=capitalize(tolower(HECHO))) %>% 
    group_by(HECHO, ANIO) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
    dcast(HECHO~ANIO,value.var="eventos") %>% 
    mutate(Diferencia20172018=`2018`-`2017`,
           variacion20172018=round(100*Diferencia20172018/`2017`,0),
           Diferencia20182019=`2019`-`2018`,
           variacion20182019=round(100*Diferencia20182019/`2018`,0),
          Diferencia20192020=`2020`-`2019`,
          variacion20192020=round(100*Diferencia20192020/`2019`,0),
          Diferencia20192021=`2021`-`2019`,
          variacion20192021=round(100*Diferencia20192021/`2019`,0),
          Diferencia20202021=`2021`-`2020`,
           variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
    arrange(desc(variacion20202021))
         write.xlsx(tabla1, "output/tabla1.xlsx", overwrite = T)
  
  #tabla 2 variaciones ordenadas por mes
  
  tabla2<-df_all %>% filter(MES==Mes, HECHO %in% hechos) %>% 
    mutate(HECHO=capitalize(tolower(HECHO))) %>% 
    group_by(HECHO, ANIO) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
    dcast(HECHO~ANIO,value.var="eventos") %>% 
    mutate(Diferencia20172018=`2018`-`2017`,
           variacion20172018=round(100*Diferencia20172018/`2017`,0),
           Diferencia20182019=`2019`-`2018`,
           variacion20182019=round(100*Diferencia20182019/`2018`,0),
           Diferencia20192020=`2020`-`2019`,
           variacion20192020=round(100*Diferencia20192020/`2019`,0),
           Diferencia20192021=`2021`-`2019`,
           variacion20192021=round(100*Diferencia20192021/`2019`,0),
           Diferencia20202021=`2021`-`2020`,
           variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
    arrange(desc(variacion20202021)) 
  write.xlsx(tabla2, "output/tabla2.xlsx", overwrite = T)  
  
  #tabla 3 orden localidad mes
  
  tabla3<-df_all %>% filter(MES==Mes, HECHO %in% hechos) %>% 
    mutate(HECHO=capitalize(tolower(HECHO)),
           LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
    group_by(HECHO, ANIO, LOCALIDAD) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
    dcast(HECHO+LOCALIDAD~ANIO,value.var="eventos") %>% 
    mutate(Diferencia20172018=`2018`-`2017`,
           variacion20172018=round(100*Diferencia20172018/`2017`,0),
           Diferencia20182019=`2019`-`2018`,
           variacion20182019=round(100*Diferencia20182019/`2018`,0),
           Diferencia20192020=`2020`-`2019`,
           variacion20192020=round(100*Diferencia20192020/`2019`,0),
           Diferencia20192021=`2021`-`2019`,
           variacion20192021=round(100*Diferencia20192021/`2019`,0),
           Diferencia20202021=`2021`-`2020`,
           variacion20202021=round(100*Diferencia20202021/`2020`,0)) 
  tabla3[is.na(tabla3)]<-0
  
  
  out<-split(tabla3, f=tabla3$HECHO)
  write.xlsx(out, "output/tabla_loca_ppt.xlsx", overwrite = T)
  
  tabla3<-tabla3 %>% group_by(HECHO) %>% 
    mutate(total_hecho19=sum(`2019`),
           total_hecho20=sum(`2020`),
           total_hecho21=sum(`2021`)) %>% 
    mutate(part_21=round(100*`2021`/total_hecho21,1),
           part_20=round(100*`2020`/total_hecho20,1),
           var_part=round(100*(part_21/part_20-1),1))
  write.xlsx(tabla3, "output/tabla3.xlsx", overwrite = T)  
  
  
  #tabla 3_1 orden localidad anio
  
  tabla3<-df_all %>% filter(MES<=Mes, HECHO %in% hechos) %>% 
    mutate(HECHO=capitalize(tolower(HECHO)),
           LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
    group_by(HECHO, ANIO, LOCALIDAD) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
    dcast(HECHO+LOCALIDAD~ANIO,value.var="eventos") %>% 
    mutate(Diferencia20172018=`2018`-`2017`,
           variacion20172018=round(100*Diferencia20172018/`2017`,0),
           Diferencia20182019=`2019`-`2018`,
           variacion20182019=round(100*Diferencia20182019/`2018`,0),
           Diferencia20192020=`2020`-`2019`,
           variacion20192020=round(100*Diferencia20192020/`2019`,0),
           Diferencia20192021=`2021`-`2019`,
           variacion20192021=round(100*Diferencia20192021/`2019`,0),
           Diferencia20202021=`2021`-`2020`,
           variacion20202021=round(100*Diferencia20202021/`2020`,0)) 
  tabla3[is.na(tabla3)]<-0
  
  tabla3<-tabla3 %>% group_by(HECHO) %>% 
    mutate(total_hecho19=sum(`2019`),
           total_hecho20=sum(`2020`),
           total_hecho21=sum(`2021`)) %>% 
    mutate(part_21=round(100*`2021`/total_hecho21,1),
           part_20=round(100*`2020`/total_hecho20,1),
           var_part=round(100*(part_21/part_20-1),1))
  write.xlsx(tabla3, "output/tabla3_1.xlsx", overwrite = T)  
  
  #tabla 4 sexo
  
  tabla4<-df_all %>% filter(MES<=Mes, HECHO %in% hechos, GENERO_PERSONA!="-") %>% 
    mutate(HECHO=capitalize(tolower(HECHO))) %>% 
      group_by(HECHO, ANIO, GENERO_PERSONA) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
    dcast(HECHO+GENERO_PERSONA~ANIO,value.var="eventos") %>% 
    mutate(Diferencia20172018=`2018`-`2017`,
           variacion20172018=round(100*Diferencia20172018/`2017`,0),
           Diferencia20182019=`2019`-`2018`,
           variacion20182019=round(100*Diferencia20182019/`2018`,0),
           Diferencia20192020=`2020`-`2019`,
           variacion20192020=round(100*Diferencia20192020/`2019`,0),
           Diferencia20192021=`2021`-`2019`,
           variacion20192021=round(100*Diferencia20192021/`2019`,0),
           Diferencia20202021=`2021`-`2020`,
           variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
    group_by(HECHO) %>% 
    mutate(total_hecho19=sum(`2019`),
           total_hecho20=sum(`2020`),
           total_hecho21=sum(`2021`)) %>% 
    mutate(part_21=round(100*`2021`/total_hecho21,1)) 
  
  tabla4$GENERO_PERSONA[tabla4$GENERO_PERSONA=="MASCULINO"]<-"Hombres"
  tabla4$GENERO_PERSONA[tabla4$GENERO_PERSONA=="FEMENINO"]<-"Mujeres"
  
  write.xlsx(tabla4, "output/tabla4.xlsx", overwrite = T)  
  
  
  
  tabla4<-df_all %>% filter(MES==Mes, HECHO %in% hechos, GENERO_PERSONA!="-") %>% 
    mutate(HECHO=capitalize(tolower(HECHO))) %>% 
    group_by(HECHO, ANIO, GENERO_PERSONA) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
    dcast(HECHO+GENERO_PERSONA~ANIO,value.var="eventos") %>% 
    mutate(Diferencia20172018=`2018`-`2017`,
           variacion20172018=round(100*Diferencia20172018/`2017`,0),
           Diferencia20182019=`2019`-`2018`,
           variacion20182019=round(100*Diferencia20182019/`2018`,0),
      Diferencia20192020=`2020`-`2019`,
           variacion20192020=round(100*Diferencia20192020/`2019`,0),
           Diferencia20192021=`2021`-`2019`,
           variacion20192021=round(100*Diferencia20192021/`2019`,0),
           Diferencia20202021=`2021`-`2020`,
           variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
    group_by(HECHO) %>% 
    mutate(total_hecho19=sum(`2019`),
           total_hecho20=sum(`2020`),
           total_hecho21=sum(`2021`)) %>% 
    mutate(part_21=round(100*`2021`/total_hecho21,1)) 
  tabla4$GENERO_PERSONA[tabla4$GENERO_PERSONA=="MASCULINO"]<-"Hombres"
  tabla4$GENERO_PERSONA[tabla4$GENERO_PERSONA=="FEMENINO"]<-"Mujeres"
  
  
  write.xlsx(tabla4, "output/tabla4_1.xlsx", overwrite = T)  
  
  
  
  # tabla 5 upz
  hechos2<-c("HOMICIDIOS", "LESIONES PERSONALES","HURTO A PERSONAS",
                    "HURTO DE CELULARES","HURTO DE BICICLETAS")
  

  tabla5<-df_all %>% filter(MES<=Mes, HECHO %in% hechos2, GENERO_PERSONA!="-") %>% 
    mutate(HECHO=capitalize(tolower(HECHO))) %>% 
    group_by(HECHO, ANIO, COD_UPZ, UPZ, LOCALIDAD) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
    dcast(HECHO+COD_UPZ+ UPZ+ LOCALIDAD~ANIO,value.var="eventos")
  tabla5[is.na(tabla5)]<-0
  tabla5<-tabla5%>% 
    mutate(Diferencia20172018=`2018`-`2017`,
           Diferencia20182019=`2019`-`2018`,
           Diferencia20192021=`2021`-`2019`,
           Diferencia20202021=`2021`-`2020`,
           variacion20172018=round(100*Diferencia20172018/`2017`,0),
           variacion20182019=round(100*Diferencia20182019/`2018`,0),
           variacion20192021=round(100*Diferencia20192021/`2019`,0),
           variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
    group_by(HECHO) %>% 
    mutate(total_hecho19=sum(`2019`),
           total_hecho20=sum(`2020`),
           total_hecho21=sum(`2021`)) %>% 
    mutate(part_21=round(100*`2021`/total_hecho21,1)) %>% 
    arrange(desc(part_21))
  
  
  write.xlsx(tabla5, "output/tabla5.xlsx", overwrite = T)  
  # tabla 6  upz mes
  hechos2<-c("HOMICIDIOS", "LESIONES PERSONALES","HURTO A PERSONAS",
             "HURTO DE CELULARES","HURTO DE BICICLETAS")
  
  
  tabla5<-df_all %>% filter(MES==Mes, HECHO %in% hechos2, GENERO_PERSONA!="-") %>% 
    mutate(HECHO=capitalize(tolower(HECHO))) %>% 
    group_by(HECHO, ANIO, COD_UPZ, UPZ, LOCALIDAD) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
    dcast(HECHO+COD_UPZ+ UPZ+ LOCALIDAD~ANIO,value.var="eventos")
  tabla5[is.na(tabla5)]<-0
  tabla5<-tabla5%>% 
    mutate(Diferencia20172018=`2018`-`2017`,
           Diferencia20182019=`2019`-`2018`,
           Diferencia20192021=`2021`-`2019`,
           Diferencia20202021=`2021`-`2020`,
           variacion20172018=round(100*Diferencia20172018/`2017`,0),
           variacion20182019=round(100*Diferencia20182019/`2018`,0),
           variacion20192021=round(100*Diferencia20192021/`2019`,0),
           variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
    group_by(HECHO) %>% 
    mutate(total_hecho19=sum(`2019`),
           total_hecho20=sum(`2020`),
           total_hecho21=sum(`2021`)) %>% 
    mutate(part_21=round(100*`2021`/total_hecho21,1)) %>% 
    arrange(desc(part_21))
  
  
  write.xlsx(tabla5, "output/tabla5_1.xlsx", overwrite = T)  
  
  # tablas jornada y rango hora para ppt
  
  df_all$HORA <- as.numeric(df_all$HORA)
  
  df_all$RANGO_DEL_DIA[df_all$HORA < 6] <- 'Madrugada'
  df_all$RANGO_DEL_DIA[df_all$HORA < 12 & df_all$HORA >= 6] <- 'Mañana'
  df_all$RANGO_DEL_DIA[df_all$HORA < 18 & df_all$HORA >= 12] <- 'Tarde'
  df_all$RANGO_DEL_DIA[df_all$HORA < 24 & df_all$HORA >= 18] <- 'Noche'
  
  DIA_SEMANA <- 1:7
  day <- c("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo")
  range_day <- c("Lun-Jue", "Lun-Jue", "Lun-Jue", "Lun-Jue", "Vie-Dom", "Vie-Dom", "Vie-Dom")
  temp <- data.frame(DIA_SEMANA, day, range_day)
  df_all <- merge(df_all, temp, all.x = TRUE)
  
  
  tabla6<-df_all %>% filter(MES<=Mes, HECHO %in% hechos) %>% 
    mutate(HECHO=capitalize(tolower(HECHO)),
           LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
    group_by(HECHO, ANIO, RANGO_DEL_DIA) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
    dcast(HECHO+RANGO_DEL_DIA~ANIO,value.var="eventos") %>% 
    mutate(Diferencia20172018=`2018`-`2017`,
           variacion20172018=round(100*Diferencia20172018/`2017`,0),
           Diferencia20182019=`2019`-`2018`,
           variacion20182019=round(100*Diferencia20182019/`2018`,0),
           Diferencia20192020=`2020`-`2019`,
           variacion20192020=round(100*Diferencia20192020/`2019`,0),
           Diferencia20192021=`2021`-`2019`,
           variacion20192021=round(100*Diferencia20192021/`2019`,0),
           Diferencia20202021=`2021`-`2020`,
           variacion20202021=round(100*Diferencia20202021/`2020`,0)) 
  tabla6[is.na(tabla6)]<-0
  
  out <- split( tabla6 , f = tabla6$HECHO )
  write.xlsx(out, "output/tabla6_horas.xlsx", overwrite = T)  
  
  tabla7<-df_all %>% filter(MES<=Mes, HECHO %in% hechos) %>% 
    mutate(HECHO=capitalize(tolower(HECHO)),
           LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
    group_by(HECHO, ANIO, range_day) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
    dcast(HECHO+range_day~ANIO,value.var="eventos") %>% 
    mutate(Diferencia20172018=`2018`-`2017`,
           variacion20172018=round(100*Diferencia20172018/`2017`,0),
           Diferencia20182019=`2019`-`2018`,
           variacion20182019=round(100*Diferencia20182019/`2018`,0),
           Diferencia20192020=`2020`-`2019`,
           variacion20192020=round(100*Diferencia20192020/`2019`,0),
           Diferencia20192021=`2021`-`2019`,
           variacion20192021=round(100*Diferencia20192021/`2019`,0),
           Diferencia20202021=`2021`-`2020`,
           variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
    rename(`Rango del día`=range_day,
           Hecho=HECHO)
  tabla7[is.na(tabla7)]<-0

  out<- split(tabla7, f=tabla7$HECHO, T)
    
  write.xlsx(out, "output/tabla7_dia.xlsx", overwrite = T)  
  
  
  # delitos en transmi
   tabla8<-df_all %>% filter(MES<=Mes, HECHO %in% hechos) %>%
     filter(grepl("TRANSM", GRUPO_CLASE_SITIO)) %>% 
     mutate(HECHO=capitalize(tolower(HECHO)),
            LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
     group_by(HECHO, ANIO) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
     dcast(HECHO~ANIO,value.var="eventos") %>% 
     mutate(Diferencia20172018=`2018`-`2017`,
            variacion20172018=round(100*Diferencia20172018/`2017`,0),
            Diferencia20182019=`2019`-`2018`,
            variacion20182019=round(100*Diferencia20182019/`2018`,0),
            Diferencia20192020=`2020`-`2019`,
            variacion20192020=round(100*Diferencia20192020/`2019`,0),
            Diferencia20192021=`2021`-`2019`,
            variacion20192021=round(100*Diferencia20192021/`2019`,0),
            Diferencia20202021=`2021`-`2020`,
            variacion20202021=round(100*Diferencia20202021/`2020`,0)) 
   tabla8[is.na(tabla8)]<-0
   
   write.xlsx(tabla8, "output/tabla_transmi.xlsx", overwrite = T)  
   
  # ranking por delitos y valores + tasas
   
   TASAS<-readxl::read_xlsx("input/poblacion_localidad.xlsx")
   TASAS<-readxl::read_xlsx("input/poblacion2018.xlsx") %>% 
     mutate(`Nombre localidad`=capitalize(tolower((`Nombre localidad`))),
       `Nombre localidad`=gsub("é", "e", `Nombre localidad`),
            `Nombre localidad`=gsub("ó", "o", `Nombre localidad`),
            `Nombre localidad`=gsub("á", "a", `Nombre localidad`)) %>% 
     rename(LOCALIDAD=`Nombre localidad`) %>% 
     select(LOCALIDAD, p2017:p2021) 
   
   tabla3<-df_all %>% filter(MES<=Mes, HECHO %in% hechos) %>% 
     mutate(HECHO=capitalize(tolower(HECHO)),
            LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
     group_by(HECHO, ANIO, LOCALIDAD) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
     dcast(HECHO+LOCALIDAD~ANIO, value.var="eventos") %>% 
     mutate(Diferencia20172018=`2018`-`2017`,
            variacion20172018=round(100*Diferencia20172018/`2017`,0),
            Diferencia20182019=`2019`-`2018`,
            variacion20182019=round(100*Diferencia20182019/`2018`,0),
            Diferencia20192020=`2020`-`2019`,
            variacion20192020=round(100*Diferencia20192020/`2019`,0),
            Diferencia20192021=`2021`-`2019`,
            variacion20192021=round(100*Diferencia20192021/`2019`,0),
            Diferencia20202021=`2021`-`2020`,
            variacion20202021=round(100*Diferencia20202021/`2020`,0)) 
   tabla3[is.na(tabla3)]<-0
   
   
   tabla3_1 <-merge(tabla3, TASAS, by="LOCALIDAD")
   
 
   tabla3_1<-tabla3_1 %>% mutate(
     tasa2017=round(`2017`*100000/p2017,1),
     tasa2018=round(`2018`*100000/p2018,1),
     tasa2019=round(`2019`*100000/p2019,1),
     tasa2020=round(`2020`*100000/p2020,1),
     tasa2021=round(`2021`*100000/p2021,1))
 
   tabla3_1<-tabla3_1 %>%  group_by(HECHO) %>% 
     mutate(rank2017 = dense_rank(desc(tasa2017)),
            rank2018 = dense_rank(desc(tasa2018)),
            rank2019 = dense_rank(desc(tasa2019)),
            rank2020 = dense_rank(desc(tasa2020)),
            rank2021 = dense_rank(desc(tasa2021))) %>% 
     select("LOCALIDAD","HECHO",
            "2017","tasa2017","rank2017",
            "2018","tasa2018","rank2018",
            "2019","tasa2019","rank2019",
            "2020","tasa2020","rank2020" ,
            "2021","tasa2021","rank2021","Diferencia20172018" ,"variacion20172018",
            "Diferencia20182019" ,"variacion20182019",
            "Diferencia20192020" ,"variacion20192020",  "Diferencia20192021",
            "variacion20192021" , "Diferencia20202021", "variacion20202021")
   
   write.xlsx(tabla3_1, "output/tabla3_tasa.xlsx", overwrite = T)
   
    out<- split(tabla3_1, f=tabla3_1$HECHO, T)
    write.xlsx(out, "output/tabla3_tasa_hojas.xlsx", overwrite = T)
   
   
   ## cosec
   temp<-df_all %>% group_by(LOCALIDAD, cosec) %>% summarise(eventos=n()) %>% 
     mutate(LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
     select(-eventos) %>% filter(!is.na(cosec)) 
   
   temp<-merge(temp, TASAS, by="LOCALIDAD") %>% group_by(cosec) %>% 
    summarise_if(is.numeric,sum)
  
  tabla_9<-df_all%>%  
    group_by(HECHO, ANIO, cosec) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>%
    filter(!is.na(cosec)) %>% 
    dcast(HECHO+cosec~ANIO, value.var="eventos") %>% 
    mutate(Diferencia20172018=`2018`-`2017`,
           variacion20172018=round(100*Diferencia20172018/`2017`,0),
           Diferencia20182019=`2019`-`2018`,
           variacion20182019=round(100*Diferencia20182019/`2018`,0),
           Diferencia20192020=`2020`-`2019`,
           variacion20192020=round(100*Diferencia20192020/`2019`,0),
           Diferencia20192021=`2021`-`2019`,
           variacion20192021=round(100*Diferencia20192021/`2019`,0),
           Diferencia20202021=`2021`-`2020`,
           variacion20202021=round(100*Diferencia20202021/`2020`,0)) 
   
  tabla_9 <-merge(tabla_9, temp, by="cosec")
  
    tabla_9<-tabla_9 %>% mutate(
    tasa2019=round(`2019`*100000/p2019,1),
    tasa2020=round(`2020`*100000/p2020,1),
    tasa2021=round(`2021`*100000/p2021,1)) %>% 
    select(-p2019,-p2020,-p2021) 
    tabla_9[is.na(tabla_9)]<-0
    
     tabla_9<-tabla_9 %>%  group_by(HECHO) %>% 
       mutate(rank2019 = dense_rank(desc(tasa2019)),
         rank2020 = dense_rank(desc(tasa2020)),
         rank2021 = dense_rank(desc(tasa2021))) %>% 
       select("cosec","HECHO","2019","tasa2019","rank2019",
              "2020","tasa2020","rank2020" ,
              "2021","tasa2021","rank2021","Diferencia20192020" ,"variacion20192020",  "Diferencia20192021",
              "variacion20192021" , "Diferencia20202021", "variacion20202021") %>% 
       mutate(HECHO=capitalize(tolower(HECHO)))
    
     
     
     write.xlsx(tabla_9, "output/tabla9_cosec.xlsx", overwrite = T)
     out<- split(tabla_9, f=tabla_9$HECHO, T)
   write.xlsx(out, "output/tabla9_cosec_hojas.xlsx", overwrite = T)
  
  #modalidad-caracteristica
   tabla10<-df_all %>% filter(MES<=Mes, HECHO %in% hechos) %>% 
     mutate(HECHO=capitalize(tolower(HECHO)),
            LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
     group_by(HECHO, ANIO, CARACTERISTICA ) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
     dcast(HECHO+CARACTERISTICA~ANIO,value.var="eventos") %>% 
     mutate(Diferencia20172018=`2018`-`2017`,
            variacion20172018=round(100*Diferencia20172018/`2017`,0),
            Diferencia20182019=`2019`-`2018`,
            variacion20182019=round(100*Diferencia20182019/`2018`,0),
            Diferencia20192020=`2020`-`2019`,
            variacion20192020=round(100*Diferencia20192020/`2019`,0),
            Diferencia20192021=`2021`-`2019`,
            variacion20192021=round(100*Diferencia20192021/`2019`,0),
            Diferencia20202021=`2021`-`2020`,
            variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
     rename(Caractaerística=CARACTERISTICA,
            Hecho=HECHO)
   tabla10[is.na(tabla10)]<-0
   out<- split(tabla10, f=tabla10$Hecho, T)
   write.xlsx(out, "output/tabla_caracteristica.xlsx", overwrite = T)
  # arma empleada
   tabla11<-df_all %>% filter(MES<=Mes, HECHO %in% hechos) %>% 
     mutate(HECHO=capitalize(tolower(HECHO)),
            LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
     group_by(HECHO, ANIO, ARMA_EMPLEADA) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
     dcast(HECHO+ARMA_EMPLEADA~ANIO,value.var="eventos") %>% 
     mutate(Diferencia20172018=`2018`-`2017`,
            variacion20172018=round(100*Diferencia20172018/`2017`,0),
            Diferencia20182019=`2019`-`2018`,
            variacion20182019=round(100*Diferencia20182019/`2018`,0),
            Diferencia20192020=`2020`-`2019`,
            variacion20192020=round(100*Diferencia20192020/`2019`,0),
            Diferencia20192021=`2021`-`2019`,
            variacion20192021=round(100*Diferencia20192021/`2019`,0),
            Diferencia20202021=`2021`-`2020`,
            variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
     rename(`arma empleada`=ARMA_EMPLEADA,
            Hecho=HECHO) %>% 
     mutate(`arma empleada`=ifelse(`arma empleada`=="ARTEFACTO EXPLOSIVO/CARGA DINAMITA",
                                   "ART. EXPLOSIVO/DINAM.", `arma empleada`)) %>% 
     arrange(variacion20202021)
   tabla11[is.na(tabla11)]<-0
   out<- split(tabla11, f=tabla11$Hecho, T)
   write.xlsx(out, "output/tabla_arma_empleada.xlsx", overwrite = T)
   
   
  
  # habitante de calle
   
   tabla12<-df_all %>% filter(MES<=Mes, HECHO %in% hechos,
                              DELITO_CARGO_PERSONA=="HABITANTE DE LA CALLE") %>% 
     mutate(HECHO=capitalize(tolower(HECHO)),
            LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
     group_by(HECHO, ANIO) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
     dcast(HECHO~ANIO,value.var="eventos") %>% 
     mutate(Diferencia20172018=`2018`-`2017`,
            variacion20172018=round(100*Diferencia20172018/`2017`,0),
            Diferencia20182019=`2019`-`2018`,
            variacion20182019=round(100*Diferencia20182019/`2018`,0),
            Diferencia20192020=`2020`-`2019`,
            variacion20192020=round(100*Diferencia20192020/`2019`,0),
            Diferencia20192021=`2021`-`2019`,
            variacion20192021=round(100*Diferencia20192021/`2019`,0),
            Diferencia20202021=`2021`-`2020`,
            variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
     rename(Hecho=HECHO) 
   tabla12[is.na(tabla12)]<-0
   write.xlsx(tabla12, "output/tabla_hab_calle.xlsx", overwrite = T)
   
  # jóvenes 14-28 
   tabla13<-df_all %>% mutate(EDAD_PERSONA=as.integer(as.character(EDAD_PERSONA))) %>% 
                                filter(MES<=Mes, HECHO %in% hechos,
                              EDAD_PERSONA>=14 & EDAD_PERSONA<=28) %>% 
     mutate(HECHO=capitalize(tolower(HECHO)),
            LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
     group_by(HECHO, ANIO) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
     dcast(HECHO~ANIO,value.var="eventos") %>% 
     mutate(Diferencia20172018=`2018`-`2017`,
            variacion20172018=round(100*Diferencia20172018/`2017`,0),
            Diferencia20182019=`2019`-`2018`,
            variacion20182019=round(100*Diferencia20182019/`2018`,0),
            Diferencia20192020=`2020`-`2019`,
            variacion20192020=round(100*Diferencia20192020/`2019`,0),
            Diferencia20192021=`2021`-`2019`,
            variacion20192021=round(100*Diferencia20192021/`2019`,0),
            Diferencia20202021=`2021`-`2020`,
            variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
     rename(Hecho=HECHO) 
   tabla13[is.na(tabla13)]<-0
   write.xlsx(tabla13, "output/tabla_jovenes.xlsx", overwrite = T)
   
   # lista de upz

   tabla3<-df_all %>% filter(MES<=Mes, HECHO %in% hechos) %>% 
     mutate(HECHO=capitalize(tolower(HECHO)),
            LOCALIDAD=capitalize(tolower(LOCALIDAD))) %>% 
     group_by(HECHO, ANIO, UPZ, LOCALIDAD) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
     dcast(HECHO+UPZ+LOCALIDAD~ANIO, value.var="eventos") 
   tabla3[is.na(tabla3)]<-0
   
  
   
   tabla3_1<-tabla3 %>%  group_by(HECHO) %>% 
     mutate(rank2017 = dense_rank(desc(`2017`)),
            rank2018 = dense_rank(desc(`2018`)),
            rank2019 = dense_rank(desc(`2019`)),
            rank2020 = dense_rank(desc(`2020`)),
            rank2021 = dense_rank(desc(`2021`))) %>% 
     select("UPZ","LOCALIDAD","HECHO","2017","rank2017",
            "2018","rank2018",
            "2019","rank2019",
            "2020","rank2020",
            "2021","rank2021") %>% 
     mutate(sumaranking=rank2017+rank2018+rank2019+rank2020+rank2021,
            dicotomica5perc=ifelse(sumaranking<=43,"1","0")) %>% 
     arrange(sumaranking)
   
   
   
   write.xlsx(tabla3_1, "output/tabla3_upz_17-21.xlsx", overwrite = T)
   
   out<- split(tabla3_1, f=tabla3_1$HECHO, T)
   write.xlsx(out, "output/tabla3_upz1721_hojas.xlsx", overwrite = T)
   
   #tabla mes2021 
   
   tabla1<-df_all %>% filter(HECHO %in% hechos) %>% 
     mutate(HECHO=capitalize(tolower(HECHO))) %>% 
     group_by(HECHO, MES,ANIO) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
     dcast(HECHO+ANIO~MES,value.var="eventos") %>% arrange(HECHO, ANIO) 
   write.xlsx(tabla1, "output/tablames.xlsx", overwrite = T)
   
   out<- split(tabla1, f=tabla1$HECHO, T)
   write.xlsx(out, "output/mes_mes_hojas.xlsx", overwrite = T)   
        # opcion 2
   tabla1<-df_all %>% filter(HECHO %in% hechos) %>% 
     mutate(HECHO=capitalize(tolower(HECHO))) %>% 
     group_by(HECHO, MES,ANIO) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
     dcast(HECHO+MES~ANIO,value.var="eventos") %>% arrange(HECHO, MES) 
   out<- split(tabla1, f=tabla1$HECHO, T)
   write.xlsx(out, "output/mes_mes_hojas_op2.xlsx", overwrite = T)   
   #transporte
   transporte<-df_all %>% filter(grepl("TRANSPORTE PUBLICO", GRUPO_CLASE_SITIO), HECHO %in% hechos) %>% 
     mutate(HECHO=capitalize(tolower(HECHO))) %>% 
     group_by(HECHO,ANIO) %>% summarise(eventos=sum(CUENTA_HECHOS)) %>% 
     dcast(HECHO~ANIO,value.var="eventos") 
   transporte[is.na(transporte)]<-0
   
   transporte<-transporte%>% 
     mutate(Diferencia20172018=`2018`-`2017`,
            variacion20172018=round(100*Diferencia20172018/`2017`,0),
            Diferencia20182019=`2019`-`2018`,
            variacion20182019=round(100*Diferencia20182019/`2018`,0),
       Diferencia20192020=`2020`-`2019`,
            variacion20192020=round(100*Diferencia20192020/`2019`,0),
            Diferencia20192021=`2021`-`2019`,
            variacion20192021=round(100*Diferencia20192021/`2019`,0),
            Diferencia20202021=`2021`-`2020`,
            variacion20202021=round(100*Diferencia20202021/`2020`,0)) %>% 
     rename(Hecho=HECHO)
     
   write.xlsx(tabla1, "output/tabla_transporte.xlsx", overwrite = T)
   
   
   
   
   