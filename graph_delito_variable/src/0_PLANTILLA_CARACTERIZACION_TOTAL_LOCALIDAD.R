######################################################DEFINIR CARPETAS#####################

# definir nombre archivo
nombreArchivoImportar <- "SIEDCO_19_20_____19_11_2020"
nombreCarpeta <- "0_CONSULTAS"
nombreSubCarpeta <- "19_11_2020_CLS_CHAPINERO"

PPrincipal<- paste0("C:/Users/rodolfo.goyeneche/Documents/0ANALISIS_DE_DATOS/", nombreCarpeta)
PArchivos <- paste0(PPrincipal,"/Archivos")
PGraficas <- paste0(PPrincipal,"/Graficas")
PBases    <- paste0(PPrincipal,"/Bases")
PScripts  <- paste0(PPrincipal,"/RScripts")

######################################################EDITAR: DEFINIR VALORES####
mes <- 10

anio_anterior <- 2019
anio_actual <- 2020

nombre_consulta <- "28_10_2020"

fecha_menor <- "2020-01-01"
fecha_mayor <- "2020-10-31"

fecha_menor_anterior <- "2019-01-01"
fecha_mayor_anterior <- "2019-10-31"


#IMPORTAR SIEDCO #####################

x  <- paste("C:/Users/rodolfo.goyeneche/Documents/0ANALISIS_DE_DATOS/0Bases/", nombreArchivoImportar, ".rds", collapse = "", sep = "")
db <- readRDS(x)
db <- data.table(db)


#EDITAR: IMPORTAR HABITANTES####

HABITANTES_BOGOTA <-  7715778 

x <- "PROYECCIONES_POBLACION_POR_UPZ"

y <- paste("C:/Users/rodolfo.goyeneche/Documents/0ANALISIS_DE_DATOS/0Bases/", x, ".xlsx",  collapse = "", sep = "")
proyecciones <- read_excel(y)
proyecciones <- data.table(proyecciones)
proyecciones <- proyecciones [, HABITANTES_UPZ := as.numeric(HABITANTES_UPZ)] 
proyecciones <- proyecciones [, COD_UPZ := as.numeric(COD_UPZ)] 

x <- "PROYECCIONES_PLOBACION_POR_LOCALIDAD_2"

y <- paste("C:/Users/rodolfo.goyeneche/Documents/0ANALISIS_DE_DATOS/0Bases/", x, ".xlsx",  collapse = "", sep = "")
proyecciones_localidad <- read_excel(y)
proyecciones_localidad <- data.table(proyecciones_localidad)
proyecciones_localidad <- proyecciones_localidad [, HABITANTES_MUJER := as.numeric(HABITANTES_MUJER)] 
proyecciones_localidad <- proyecciones_localidad [, HABITANTES_HOMBRE := as.numeric(HABITANTES_HOMBRE)] 
proyecciones_localidad <- proyecciones_localidad [, HABITANTES := as.numeric(HABITANTES)] 




#MODIFICAR VARIABLES####

db <- db [, FECHA_HECHO := as.Date(as.character(FECHA_HECHO))]
db <- db [, MES := as.numeric(as.character(MES))]
db <- db [, ANIO := as.numeric(as.character(ANIO))]
db <- db [LOCALIDAD == "ANTONIO NARI?O", LOCALIDAD := "ANTONIO NARINO"]
db <- db [, COD_UPZ := substr(COD_UPZ, 4, 7)]
db <- db [, COD_UPZ := as.numeric(COD_UPZ)] 

#DEFINIR DELITOS####


TDelitos<- c("AMENAZAS",
             "DELITOS SEXUALES",
             "EXTORSION",
             "HOMICIDIOS",
             "HURTO A PERSONAS",
             "HURTO A RESIDENCIAS",
             "HURTO AUTOMOTORES",
             "HURTO DE BICICLETAS",
             "HURTO DE CELULARES",
             "HURTO MOTOCICLETAS",
             "LESIONES PERSONALES",
             "VIOLENCIA INTRAFAMILIAR",
             "HURTO A COMERCIO")


#DEFINIR LOCALIDADES####
TLocalidades<- c("ANTONIO NARINO",
                 "BARRIOS UNIDOS",
                 "BOSA",
                 "CANDELARIA",
                 "CHAPINERO",
                 "CIUDAD BOLIVAR",
                 "ENGATIVA",
                 "FONTIBON",
                 "KENNEDY",
                 "LOS MARTIRES",
                 "PUENTE ARANDA",
                 "RAFAEL URIBE URIBE",
                 "SAN CRISTOBAL",
                 "SANTA FE",
                 "SUBA",
                 "SUMAPAZ",
                 "TEUSAQUILLO",
                 "TUNJUELITO",
                 "USAQUEN",
                 "USME")



######################################################FILTRAR POR DELITOS####
db <- db [HECHO != "HOMICIDIOS EN AT"] 
db <- db [HECHO != "HURTO DE CELULARES (CASOS)"] 
db <- db [HECHO != "HURTO PIRATERIA TERRESTRE"] 
db <- db [HECHO != "HURTO DE CELULARES (CASOS)"] 
db <- db [HECHO != "LESIONES EN AT"] 


######################################################FILTRAR POR FECHA####
db <- db [(ANIO == anio_anterior) | (ANIO == anio_actual & MES <= mes)]
table(db$MES, db$ANIO)


#GRAFICA MENSUAL LOCALIDADES#####################################

db <- db [MES == 1, MES_ESCRITO := "ENE"]
db <- db [MES == 2, MES_ESCRITO := "FEB"]
db <- db [MES == 3, MES_ESCRITO := "MAR"]
db <- db [MES == 4, MES_ESCRITO := "ABR"]
db <- db [MES == 5, MES_ESCRITO := "MAY"]
db <- db [MES == 6, MES_ESCRITO := "JUN"]
db <- db [MES == 7, MES_ESCRITO := "JUL"]
db <- db [MES == 8, MES_ESCRITO := "AGO"]
db <- db [MES == 9, MES_ESCRITO := "SEP"]
db <- db [MES == 10, MES_ESCRITO := "OCT"]
db <- db [MES == 11, MES_ESCRITO := "NOV"]
db <- db [MES == 12, MES_ESCRITO := "DIC"]

db <- db [, LABEL := paste(ANIO, MES_ESCRITO)]

db$LABEL <- factor(db$LABEL, levels = c("2019 ENE", 
                                        "2019 FEB",
                                        "2019 MAR", 
                                        "2019 ABR",
                                        "2019 MAY",
                                        "2019 JUN",
                                        "2019 JUL",
                                        "2019 AGO",
                                        "2019 SEP",
                                        "2019 OCT",
                                        "2019 NOV",
                                        "2019 DIC",
                                        "2020 ENE",
                                        "2020 FEB",
                                        "2020 MAR",
                                        "2020 ABR",
                                        "2020 MAY",
                                        "2020 JUN",
                                        "2020 JUL",
                                        "2020 AGO",
                                        "2020 SEP",
                                        "2020 OCT",
                                        "2020 NOV",
                                        "2020 DIC"))

evolucion_mensual <- function (delito, localidad){
  
  db_delito <- db [HECHO == delito & LOCALIDAD ==localidad]
  
  ResumenGraficas<- db_delito[,list(Eventos=sum(CUENTA_HECHOS)),by=c("LABEL")]
  L1 <- unique(db$LABEL)
  EmptyPar0 <- as.data.table(as.data.frame(expand.grid(LABEL=(L1))))
  EmptyPar0<- merge(EmptyPar0,ResumenGraficas, by=c("LABEL"),all=T)
  for (j in names(EmptyPar0))
    set(EmptyPar0,which(is.na(EmptyPar0[[j]])),j,0)
  
  ##### Generar LOOP para histogramas por delito
  
  df <- EmptyPar0
  # El dataframe df es un total de delito por mes y a�o
  p <- ggplot(data=df, aes(x= LABEL, y=Eventos)) +
    geom_bar(stat="identity", fill="#fbb724")+
    theme_minimal() +
    theme_classic() +
    geom_text(aes(label=Eventos),colour="white", angle=90,
              position=position_dodge(width=1),
              vjust=0.5, hjust=1, size=6) +
    xlab(element_blank())+
    ylab(element_blank())+
    theme(axis.line.y = element_blank())+
    theme(axis.ticks = element_blank())+
    theme(axis.text.x = element_text(angle = 90))+
    theme(axis.text=element_text(size=10))+
    theme(axis.text.y=element_blank())
  p
  ggsave(paste0(PPrincipal, "/", nombreSubCarpeta, "/" , "0. GRAFICA_EVOLUCION_MENSUAL_", localidad, "_", delito, ".jpeg"), plot = last_plot(),width = 11.29, height = 7.02, units = "cm")
}

#### Generacion de Graficas por Tipo de Delito y Distancia.

for(i in TDelitos)
{for (j in TLocalidades) {
  evolucion_mensual(i, j)
}}












######################################################FILTRAR POR FECHA####
db <- db[(FECHA_HECHO >= fecha_menor_anterior & FECHA_HECHO <= fecha_mayor_anterior) 
         | (FECHA_HECHO >= fecha_menor & FECHA_HECHO <= fecha_mayor)]

table(db$MES, db$ANIO)




#TABLERO CONTROL GENERAL#####

funcion <- function (localidad){
  
  db_0 <- db[LOCALIDAD == localidad & GENERO_PERSONA == "FEMENINO" & ANIO == anio_anterior, list(Eventos=sum(CUENTA_HECHOS)), by=c("HECHO")]
  db_1 <- db[LOCALIDAD == localidad & GENERO_PERSONA == "FEMENINO" & ANIO == anio_actual, list(Eventos=sum(CUENTA_HECHOS)), by=c("HECHO")]
  db_2 <- db[LOCALIDAD == localidad & GENERO_PERSONA == "MASCULINO" & ANIO == anio_anterior, list(Eventos=sum(CUENTA_HECHOS)), by=c("HECHO")]
  db_3 <- db[LOCALIDAD == localidad & GENERO_PERSONA == "MASCULINO" & ANIO == anio_actual, list(Eventos=sum(CUENTA_HECHOS)), by=c("HECHO")]
  db_4 <- db[LOCALIDAD == localidad & ANIO == anio_anterior, list(Eventos=sum(CUENTA_HECHOS)), by=c("HECHO")]
  db_5 <- db[LOCALIDAD == localidad & ANIO == anio_actual, list(Eventos=sum(CUENTA_HECHOS)), by=c("HECHO")]
  
  HECHOS <- unique(db$HECHO)
  EmptyPar0 <- as.data.table(as.data.frame(expand.grid(HECHO=(HECHOS))))
  
  db_semaforo <- merge(EmptyPar0  , db_0, by=c("HECHO"),all.x=T)
  db_semaforo <- merge(db_semaforo, db_1, by=c("HECHO"),all.x=T)
  db_semaforo <- db_semaforo [is.na(Eventos.x), Eventos.x := 0]
  db_semaforo <- db_semaforo [is.na(Eventos.y), Eventos.y := 0]
  names(db_semaforo)[names(db_semaforo) == "Eventos.x"] <- "FEMENINO_1"
  names(db_semaforo)[names(db_semaforo) == "Eventos.y"] <- "FEMENINO_2"
  db_semaforo <- db_semaforo[, DIFERENCIA := FEMENINO_2 - FEMENINO_1]
  db_semaforo <- db_semaforo[ FEMENINO_1 != 0, VARIACION := (DIFERENCIA  / FEMENINO_1 ) * 100]
  db_semaforo$VARIACION  <- round(db_semaforo$VARIACION, 0)
  db_semaforo <- db_semaforo[ FEMENINO_1 == 0 & (FEMENINO_2 > FEMENINO_1), VARIACION := 100]
  db_semaforo <- db_semaforo[ FEMENINO_1 == 0 & FEMENINO_2 == 0, VARIACION := 0]
  db_semaforo <- db_semaforo[ FEMENINO_1 == 0 & (FEMENINO_2 < FEMENINO_1), VARIACION :=  - 100]
  db_semaforo <- db_semaforo[, VARIACION :=  paste(VARIACION, "%")]
  db_semaforo <- merge(db_semaforo, db_2, by=c("HECHO"),all.x=T)
  db_semaforo <- merge(db_semaforo, db_3, by=c("HECHO"),all.x=T)
  db_semaforo <- db_semaforo [is.na(Eventos.x), Eventos.x := 0]
  db_semaforo <- db_semaforo [is.na(Eventos.y), Eventos.y := 0]
  names(db_semaforo)[names(db_semaforo) == "Eventos.x"] <- "MASCULINO_1"
  names(db_semaforo)[names(db_semaforo) == "Eventos.y"] <- "MASCULINO_2"
  db_semaforo <- db_semaforo[, DIFERENCIA_2 := MASCULINO_2 - MASCULINO_1]
  db_semaforo <- db_semaforo[ MASCULINO_1 != 0, VARIACION_2 := (DIFERENCIA_2  / MASCULINO_1 ) * 100]
  db_semaforo$VARIACION_2  <- round(db_semaforo$VARIACION_2, 0)
  db_semaforo <- db_semaforo[ MASCULINO_1 == 0 & (MASCULINO_2 > MASCULINO_1), VARIACION_2 := 100]
  db_semaforo <- db_semaforo[ MASCULINO_1 == 0 & MASCULINO_2 == 0, VARIACION_2 := 0]
  db_semaforo <- db_semaforo[ MASCULINO_1 == 0 & (MASCULINO_2 < MASCULINO_1), VARIACION_3 :=  - 100]
  db_semaforo <- db_semaforo[, VARIACION_2 :=  paste(VARIACION_2, "%")]
  db_semaforo <- merge(db_semaforo, db_4, by=c("HECHO"),all.x=T)
  db_semaforo <- merge(db_semaforo, db_5, by=c("HECHO"),all.x=T)
  db_semaforo <- db_semaforo [is.na(Eventos.x), Eventos.x := 0]
  db_semaforo <- db_semaforo [is.na(Eventos.y), Eventos.y := 0]
  names(db_semaforo)[names(db_semaforo) == "Eventos.x"] <- "TOTAL_1"
  names(db_semaforo)[names(db_semaforo) == "Eventos.y"] <- "TOTAL_2"
  db_semaforo <- db_semaforo[, DIFERENCIA_3 := TOTAL_2 - TOTAL_1]
  db_semaforo <- db_semaforo[ TOTAL_1 != 0, VARIACION_3 := (DIFERENCIA_3  / TOTAL_1 ) * 100]
  db_semaforo$VARIACION_3  <- round(db_semaforo$VARIACION_3, 0)
  db_semaforo <- db_semaforo[ TOTAL_1 == 0 & (TOTAL_2 > TOTAL_1), VARIACION_3 := 100]
  db_semaforo <- db_semaforo[ TOTAL_1 == 0 & TOTAL_2 == 0, VARIACION_3 := 0]
  db_semaforo <- db_semaforo[ TOTAL_1 == 0 & (TOTAL_2 < TOTAL_1), VARIACION_3 :=  - 100]
  db_semaforo <- db_semaforo[, VARIACION_3 :=  paste(VARIACION_3, "%")]
  
  setcolorder(db_semaforo, c("HECHO", "FEMENINO_1", "FEMENINO_2", "DIFERENCIA", "VARIACION",
                             "MASCULINO_1", "MASCULINO_2", "DIFERENCIA_2", "VARIACION_2",
                             "TOTAL_1", "TOTAL_2", "DIFERENCIA_3", "VARIACION_3"))
  
  names(db_semaforo)[names(db_semaforo) == "FEMENINO_1"] <- "FEMENINO 2019"
  names(db_semaforo)[names(db_semaforo) == "FEMENINO_2"] <- "FEMENINO 2020"
  names(db_semaforo)[names(db_semaforo) == "DIFERENCIA"] <- "DIFERENCIA FEMENINO"
  names(db_semaforo)[names(db_semaforo) == "VARIACION"] <- "VARIACI�N FEMENINO"
  names(db_semaforo)[names(db_semaforo) == "MASCULINO_1"] <- "MASCULINO 2019"
  names(db_semaforo)[names(db_semaforo) == "MASCULINO_2"] <- "MASCULINO 2020"
  names(db_semaforo)[names(db_semaforo) == "DIFERENCIA_2"] <- "DIFERENCIA MASCULINO"
  names(db_semaforo)[names(db_semaforo) == "VARIACION_2"] <- "VARIACI�N MASCULINO"
  names(db_semaforo)[names(db_semaforo) == "TOTAL_1"] <- "TOTAL 2019"
  names(db_semaforo)[names(db_semaforo) == "TOTAL_2"] <- "TOTAL 2020"
  names(db_semaforo)[names(db_semaforo) == "DIFERENCIA_3"] <- "DIFERENCIA TOTAL"
  names(db_semaforo)[names(db_semaforo) == "VARIACION_3"] <- "VARIACI�N TOTAL"
  
  db_semaforo
  
  write.csv2(db_semaforo, paste (PPrincipal, "/", nombreSubCarpeta, "/" , "0. TABLERO_CONTROL_", localidad, ".csv",  collapse = "", sep = ""), row.names = FALSE)
  
}

#### Generacion de Graficas por Tipo de Delito y Distancia.

for(i in TLocalidades){
  funcion(i)
}



######################################################FILTRAR POR FECHA####
db <- db[FECHA_HECHO >= fecha_menor & FECHA_HECHO <= fecha_mayor]

table(db$MES, db$ANIO)


#GRAFICA GENERO#####################################

funcion <- function (delito, localidad){
  
  db_localidad <- db [HECHO == delito & LOCALIDAD ==localidad & GENERO_PERSONA != "-"]
  
  db_localidad<- db_localidad[,list(Eventos=sum(CUENTA_HECHOS)),by=c("GENERO_PERSONA")]
  
  ResumenGraficas <- db_localidad [sum(Eventos) != 0 , Eventos := ((Eventos / sum(Eventos))* 100 )]
  ResumenGraficas <- db_localidad [sum(Eventos) == 0 , Eventos := 0]
  ResumenGraficas$Eventos <- round(ResumenGraficas$Eventos, 0)
  
  L1 <- unique(db$GENERO_PERSONA)
  EmptyPar0 <- as.data.table(as.data.frame(expand.grid(GENERO_PERSONA=(L1))))
  EmptyPar0<- merge(EmptyPar0,ResumenGraficas, by=c("GENERO_PERSONA"),all=T)
  for (j in names(EmptyPar0))
    set(EmptyPar0,which(is.na(EmptyPar0[[j]])),j,0)
  
  ##### Generar LOOP para histogramas por delito
  
  df <- EmptyPar0
  # El dataframe df es un total de delito por mes y a�o
  p <- ggplot(data=df, aes(x= GENERO_PERSONA, y=Eventos)) +
    geom_bar(stat="identity", fill="#0f2147")+
    theme_minimal() +
    theme_classic() +
    geom_text(aes(label= paste (Eventos, "%")),colour="white", angle=90,
              position=position_dodge(width=1),
              vjust=0.5, hjust=1, size=6) +
    xlab(element_blank())+
    ylab(element_blank())+
    theme(axis.line.y = element_blank())+
    theme(axis.ticks = element_blank())+
    theme(axis.text=element_text(size=10))+
    theme(axis.text.y=element_blank()) 
  p
  ggsave(paste0(PPrincipal, "/", nombreSubCarpeta, "/" , "1. GRAFICA_GENERO_",localidad, "_", delito, ".jpeg"), plot = last_plot(),width = 11.29, height = 7.02, units = "cm")
}

#### Generacion de Graficas por Tipo de Delito y Distancia.

for(i in TDelitos)
{for (j in TLocalidades) {
  funcion(i, j)
}}










#GRAFICA RANGO EDAD#####################################
db <- db [RANGO_EDAD != "-"]

db <- db [RANGO_EDAD == "0 - 4 A?OS", RANGO_EDAD := substr(RANGO_EDAD, 0, 5)]
db <- db [RANGO_EDAD == "5 - 9 A?OS", RANGO_EDAD := substr(RANGO_EDAD, 0, 5)]
db <- db [, RANGO_EDAD := substr(RANGO_EDAD, 0, 7)]

db$RANGO_EDAD <- factor(db$RANGO_EDAD, levels = c("0 - 4",
                                                  "5 - 9",
                                                  "10 - 14",
                                                  "15 - 19",
                                                  "20 - 24",
                                                  "25 - 29",
                                                  "30 - 34",
                                                  "35 - 39",
                                                  "40 - 44",
                                                  "45 - 49",
                                                  "50 - 54",
                                                  "55 - 59",
                                                  "60 - 64",
                                                  "65 - 69",
                                                  "70 - 74",
                                                  "75 - 79",
                                                  "80 - 84",
                                                  "85 - 89",
                                                  "90 - 94"))


funcion <- function (delito, localidad){
  
  db_localidad <- db [HECHO == delito  & LOCALIDAD ==localidad]
  
  db_localidad<- db_localidad[,list(Eventos=sum(CUENTA_HECHOS)),by=c("RANGO_EDAD")]
  
  ResumenGraficas <- db_localidad [sum(Eventos) != 0 , Eventos := ((Eventos / sum(Eventos))* 100 )]
  ResumenGraficas <- db_localidad [sum(Eventos) == 0 , Eventos := 0]
  ResumenGraficas$Eventos <- round(ResumenGraficas$Eventos, 0)
  
  L1 <- unique(db$RANGO_EDAD)
  EmptyPar0 <- as.data.table(as.data.frame(expand.grid(RANGO_EDAD=(L1))))
  EmptyPar0<- merge(EmptyPar0,ResumenGraficas, by=c("RANGO_EDAD"),all=T)
  
  
  
  ##### Generar LOOP para histogramas por delito
  
  df <- EmptyPar0
  # El dataframe df es un total de delito por mes y a�o
  p <- ggplot(data=df, aes(x= RANGO_EDAD, y=Eventos)) +
    geom_bar(stat="identity", fill="#0f2147")+
    theme_minimal() +
    theme_classic() +
    geom_text(aes(label= paste (Eventos, "%")),colour="white", angle=90,
              position=position_dodge(width=1),
              vjust=0.5, hjust=1, size=3) +
    xlab(element_blank())+
    ylab(element_blank())+
    theme(axis.line.y = element_blank())+
    theme(axis.ticks = element_blank())+
    theme(axis.text.x = element_text(angle = 90))+
    theme(axis.text=element_text(size=6))+
    theme(axis.text.y=element_blank())
  p
  ggsave(paste0(PPrincipal, "/", nombreSubCarpeta, "/" , "2. GRAFICA_RANGO_EDAD_",localidad, "_", delito, ".jpeg"), plot = last_plot(),width = 11.29, height = 7.02, units = "cm")
  
}

#### Generacion de Graficas por Tipo de Delito y Distancia.

for(i in TDelitos)
{for (j in TLocalidades) {
  funcion(i, j)
}}










#GRAFICA RANGO HORA#####################################
db$RANGO_HORA <- factor(db$RANGO_HORA, levels = c("00:00 - 02:59", 
                                                  "03:00 - 05:59", 
                                                  "06:00 - 08:59", 
                                                  "09:00 - 11:59", 
                                                  "12:00 - 14:59",
                                                  "15:00 - 17:59",
                                                  "18:00 - 20:59",
                                                  "21:00 - 23:59"))

funcion <- function (delito, localidad){
  
  db_localidad <- db [HECHO == delito & LOCALIDAD ==localidad]
  
  db_localidad<- db_localidad[,list(Eventos=sum(CUENTA_HECHOS)),by=c("RANGO_HORA")]
  
  ResumenGraficas <- db_localidad [sum(Eventos) != 0 , Eventos := ((Eventos / sum(Eventos))* 100 )]
  ResumenGraficas <- db_localidad [sum(Eventos) == 0 , Eventos := 0]
  ResumenGraficas$Eventos <- round(ResumenGraficas$Eventos, 0)
  
  L1 <- unique(db$RANGO_HORA)
  EmptyPar0 <- as.data.table(as.data.frame(expand.grid(RANGO_HORA=(L1))))
  EmptyPar0<- merge(EmptyPar0,ResumenGraficas, by=c("RANGO_HORA"),all=T)
  
  
  
  ##### Generar LOOP para histogramas por delito
  
  df <- EmptyPar0
  # El dataframe df es un total de delito por mes y a�o
  p <- ggplot(data=df, aes(x= RANGO_HORA, y=Eventos)) +
    geom_bar(stat="identity", fill="#0f2147")+
    theme_minimal() +
    theme_classic() +
    geom_text(aes(label= paste (Eventos, "%")),colour="white", angle=90,
              position=position_dodge(width=1),
              vjust=0.5, hjust=1, size=3) +
    xlab(element_blank())+
    ylab(element_blank())+
    theme(axis.line.y = element_blank())+
    theme(axis.ticks = element_blank())+
    theme(axis.text.x = element_text(angle = 90))+
    theme(axis.text=element_text(size=6))+
    theme(axis.text.y=element_blank())
  p
  ggsave(paste0(PPrincipal, "/", nombreSubCarpeta, "/" , "3. GRAFICA_RANGO_HORA_",localidad, "_", delito, ".jpeg"), plot = last_plot(),width = 11.29, height = 7.02, units = "cm")
  
}

#### Generacion de Graficas por Tipo de Delito y Distancia.

for(i in TDelitos)
{for (j in TLocalidades) {
  funcion(i, j)
}}











#GRAFICA DIA SEMANA#####################################
db [, DIA_ESCRITO:=weekdays(as.Date(FECHA_HECHO))]



db$DIA_ESCRITO <- factor(db$DIA_ESCRITO, levels = c("lunes", 
                                                    "martes", 
                                                    "mi�rcoles",
                                                    "jueves",
                                                    "viernes",
                                                    "s�bado",
                                                    "domingo"))

funcion <- function (delito, localidad){
  
  db_localidad <- db [HECHO == delito & LOCALIDAD ==localidad]
  
  db_localidad<- db_localidad[,list(Eventos=sum(CUENTA_HECHOS)),by=c("DIA_ESCRITO")]
  
  ResumenGraficas <- db_localidad [sum(Eventos) != 0 , Eventos := ((Eventos / sum(Eventos))* 100 )]
  ResumenGraficas <- db_localidad [sum(Eventos) == 0 , Eventos := 0]
  ResumenGraficas$Eventos <- round(ResumenGraficas$Eventos, 0)
  
  L1 <- unique(db$DIA_ESCRITO)
  EmptyPar0 <- as.data.table(as.data.frame(expand.grid(DIA_ESCRITO=(L1))))
  EmptyPar0<- merge(EmptyPar0,ResumenGraficas, by=c("DIA_ESCRITO"),all=T)
  
  
  
  ##### Generar LOOP para histogramas por delito
  
  df <- EmptyPar0
  # El dataframe df es un total de delito por mes y a�o
  p <- ggplot(data=df, aes(x= DIA_ESCRITO, y=Eventos)) +
    geom_bar(stat="identity", fill="#0f2147")+
    theme_minimal() +
    theme_classic() +
    geom_text(aes(label= paste (Eventos, "%")),colour="white", angle=90,
              position=position_dodge(width=1),
              vjust=0.5, hjust=1, size=3) +
    xlab(element_blank())+
    ylab(element_blank())+
    theme(axis.line.y = element_blank())+
    theme(axis.ticks = element_blank())+
    theme(axis.text.x = element_text(angle = 90))+
    theme(axis.text=element_text(size=6))+
    theme(axis.text.y=element_blank())
  p
  ggsave(paste0(PPrincipal, "/", nombreSubCarpeta, "/" , "4. GRAFICA_DIA_SEMANA_",localidad, "_", delito, ".jpeg"), plot = last_plot(),width = 11.29, height = 7.02, units = "cm")
  
}

#### Generacion de Graficas por Tipo de Delito y Distancia.

for(i in TDelitos)
{for (j in TLocalidades) {
  funcion(i, j)
}}










#GRAFICA UPZ#####################################


funcion <- function (delito, localidad){
  
  db_localidad <- db [HECHO == delito & LOCALIDAD ==localidad]
  
  db_localidad<- db_localidad[,list(Eventos=sum(CUENTA_HECHOS)),by=c("UPZ")]
  
  ResumenGraficas <- db_localidad [sum(Eventos) != 0 , Eventos := ((Eventos / sum(Eventos))* 100 )]
  ResumenGraficas <- db_localidad [sum(Eventos) == 0 , Eventos := 0]
  ResumenGraficas$Eventos <- round(ResumenGraficas$Eventos, 0)
  
  L1 <- unique(db$UPZ)
  EmptyPar0 <- as.data.table(as.data.frame(expand.grid(UPZ=(L1))))
  EmptyPar0<- merge(EmptyPar0,ResumenGraficas, by=c("UPZ"),all=T)
  
  
  
  ##### Generar LOOP para histogramas por delito
  
  df <- EmptyPar0
  # El dataframe df es un total de delito por mes y a�o
  p <- ggplot(data=df, aes(x= reorder(UPZ,-Eventos), y=Eventos)) +
    geom_bar(stat="identity", fill="#0f2147")+
    theme_minimal() +
    theme_classic() +
    geom_text(aes(label= paste (Eventos, "%")),colour="white", angle=90,
              position=position_dodge(width=1),
              vjust=0.5, hjust=1, size=3) +
    xlab(element_blank())+
    ylab(element_blank())+
    theme(axis.line.y = element_blank())+
    theme(axis.ticks = element_blank())+
    theme(axis.text.x = element_text(angle = 90))+
    theme(axis.text=element_text(size=6))+
    theme(axis.text.y=element_blank())
  p
  ggsave(paste0(PPrincipal, "/", nombreSubCarpeta, "/" , "5. GRAFICA_UPZ_",localidad, "_", delito, ".jpeg"), plot = last_plot(),width = 11.29, height = 7.02, units = "cm")
  
}

#### Generacion de Graficas por Tipo de Delito y Distancia.

for(i in TDelitos)
{for (j in TLocalidades) {
  funcion(i, j)
}}









#GRAFICA ARMA EMPLEADA#####################################

funcion <- function (delito, localidad){
  
  db_localidad <- db [HECHO == delito & LOCALIDAD ==localidad]
  
  db_localidad<- db_localidad[,list(Eventos=sum(CUENTA_HECHOS)),by=c("ARMA_EMPLEADA")]
  
  ResumenGraficas <- db_localidad [sum(Eventos) != 0 , Eventos := ((Eventos / sum(Eventos))* 100 )]
  ResumenGraficas <- db_localidad [sum(Eventos) == 0 , Eventos := 0]
  ResumenGraficas$Eventos <- round(ResumenGraficas$Eventos, 0)
  
  
  ##### Generar LOOP para histogramas por delito
  
  df <- ResumenGraficas
  # El dataframe df es un total de delito por mes y a�o
  p <- ggplot(data=df, aes(x= reorder (ARMA_EMPLEADA, -Eventos), y=Eventos)) +
    geom_bar(stat="identity", fill="#0f2147")+
    theme_minimal() +
    theme_classic() +
    geom_text(aes(label= paste (Eventos, "%")),colour="white", angle=90,
              position=position_dodge(width=1),
              vjust=0.5, hjust=1, size=3) +
    xlab(element_blank())+
    ylab(element_blank())+
    theme(axis.line.y = element_blank())+
    theme(axis.ticks = element_blank())+
    theme(axis.text.x = element_text(angle = 90))+
    theme(axis.text=element_text(size=6))+
    theme(axis.text.y=element_blank())
  p
  ggsave(paste0(PPrincipal, "/", nombreSubCarpeta, "/" , "6. ARMA_EMPLEADA_",localidad, "_", delito, ".jpeg"), plot = last_plot(),width = 11.29, height = 7.02, units = "cm")
}

#### Generacion de Graficas por Tipo de Delito y Distancia.

for(i in TDelitos)
{for (j in TLocalidades) {
  funcion(i, j)
}}










#GRAFICA MOVIL AGRESOR#####################################

funcion <- function (delito, localidad){
  
  db_localidad <- db [HECHO == delito & LOCALIDAD ==localidad]
  
  db_localidad<- db_localidad[,list(Eventos=sum(CUENTA_HECHOS)),by=c("MOVIL_AGRESOR")]
  
  ResumenGraficas <- db_localidad [sum(Eventos) != 0 , Eventos := ((Eventos / sum(Eventos))* 100 )]
  ResumenGraficas <- db_localidad [sum(Eventos) == 0 , Eventos := 0]
  ResumenGraficas$Eventos <- round(ResumenGraficas$Eventos, 0)
  
  
  ##### Generar LOOP para histogramas por delito
  
  df <- ResumenGraficas
  # El dataframe df es un total de delito por mes y a�o
  p <- ggplot(data=df, aes(x= reorder (MOVIL_AGRESOR, -Eventos), y=Eventos)) +
    geom_bar(stat="identity", fill="#0f2147")+
    theme_minimal() +
    theme_classic() +
    geom_text(aes(label= paste (Eventos, "%")),colour="white", angle=90,
              position=position_dodge(width=1),
              vjust=0.5, hjust=1, size=3) +
    xlab(element_blank())+
    ylab(element_blank())+
    theme(axis.line.y = element_blank())+
    theme(axis.ticks = element_blank())+
    theme(axis.text.x = element_text(angle = 90))+
    theme(axis.text=element_text(size=6))+
    theme(axis.text.y=element_blank())
  p
  ggsave(paste0(PPrincipal, "/", nombreSubCarpeta, "/" , "7. GRAFICA_MOVIL_AGRESOR_",localidad, "_", delito, ".jpeg"), plot = last_plot(),width = 11.29, height = 7.02, units = "cm")
}

#### Generacion de Graficas por Tipo de Delito y Distancia.

for(i in TDelitos)
{for (j in TLocalidades) {
  funcion(i, j)
}}










#GRAFICA COMPARACI�N TASAS####

funcion <- function (localidad, delito){
  db_localidad <- db [LOCALIDAD == localidad & UPZ != "EL MOCHUELO"
                      & UPZ != "AEROPUERTO EL DORADO" & UPZ != "PARQUE SIMON BOLIVAR - CAN"]
  L1 <- unique(db_localidad$UPZ)
  
  db_localidad <- db [LOCALIDAD == localidad & HECHO == delito & UPZ != "EL MOCHUELO"
                      & UPZ != "AEROPUERTO EL DORADO" & UPZ != "PARQUE SIMON BOLIVAR - CAN"]
  
  db_localidad<- db_localidad[,list(Eventos=sum(CUENTA_HECHOS)),by=c("COD_UPZ", "UPZ")]
  db_localidad<- merge(db_localidad,proyecciones, by=c("COD_UPZ"),all.x=T)
  
  ResumenGraficas <- db_localidad [, TASA := ((Eventos / HABITANTES_UPZ)* 100000 )]
  ResumenGraficas$TASA <- round(ResumenGraficas$TASA, 0)
  
  
  EmptyPar0 <- as.data.table(as.data.frame(expand.grid(UPZ=(L1))))
  EmptyPar0<- merge(EmptyPar0,ResumenGraficas, by=c("UPZ"),all=T)
  for (j in names(EmptyPar0))
    set(EmptyPar0,which(is.na(EmptyPar0[[j]])),j,0)
  
  EmptyPar0 = subset(EmptyPar0, select = -c(COD_UPZ, Eventos, HABITANTES_UPZ) )
  ResumenGraficas1 <- EmptyPar0
  
  db_localidad <- db [LOCALIDAD == localidad & HECHO == delito]
  
  db_localidad<- db_localidad[,list(Eventos=sum(CUENTA_HECHOS)),by=c("LOCALIDAD")]
  db_localidad<- merge(db_localidad,proyecciones_localidad, by=c("LOCALIDAD"),all.x=T)
  
  ResumenGraficas2 <- db_localidad [, TASA := ((Eventos / HABITANTES)* 100000 )]
  ResumenGraficas2$TASA <- round(ResumenGraficas2$TASA, 0)
  
  ResumenGraficas2 = subset(ResumenGraficas2, select = -c(Eventos, HABITANTES_MUJER, HABITANTES_HOMBRE, HABITANTES) )
  
  names(ResumenGraficas2)[names(ResumenGraficas2) == "LOCALIDAD"] <-  "UPZ"
  ResumenGraficas2 <- ResumenGraficas2[UPZ == localidad, UPZ := "--LOCALIDAD--"]
  
  valor_localidad <- unique(ResumenGraficas2$TASA)
  
  db_localidad <- db [HECHO == delito]
  
  db_localidad<- db_localidad[,list(Eventos=sum(CUENTA_HECHOS)),by=c("HECHO")]
  
  ResumenGraficas3 <- db_localidad [, TASA := ((Eventos / HABITANTES_BOGOTA)* 100000 )]
  ResumenGraficas3$TASA <- round(ResumenGraficas3$TASA, 0)
  
  ResumenGraficas3 = subset(ResumenGraficas3, select = -c(Eventos) )
  names(ResumenGraficas3)[names(ResumenGraficas3) == "HECHO"] <-  "UPZ"
  
  ResumenGraficas3 <- ResumenGraficas3[UPZ == delito, UPZ := "--BOGOT�--"]
  
  valor_bogota <- unique(ResumenGraficas3$TASA)
  
  
  df <- rbind(ResumenGraficas1, ResumenGraficas2, ResumenGraficas3)
  df <- df [UPZ != "-"]
  
  
  
  # El dataframe df es un total de delito por mes y a�o
  p <- ggplot(data=df, aes(x= UPZ, y=TASA)) +
    geom_bar(stat="identity", fill="#b13361")+
    theme_minimal() +
    theme_classic() +
    geom_text(aes(label=TASA),colour="white",  
              position=position_dodge(width=1),
              vjust=1, size=6) +
    xlab(element_blank())+
    ylab(element_blank())+
    theme(axis.line.y = element_blank())+
    theme(axis.ticks = element_blank())+
    theme(axis.text.x = element_text(angle = 90))+
    theme(axis.text=element_text(size=10))+
    theme(axis.text.y=element_blank())+
    geom_hline(yintercept = valor_bogota, linetype = "dashed", color= "blue", na.rm = TRUE)+
    geom_hline(yintercept = valor_localidad, linetype = "dashed", color= "blue", na.rm = TRUE)
  p
  ggsave(paste0(PPrincipal, "/", nombreSubCarpeta, "/" , "8. GRAFICA_TASAS_",localidad, "_", delito, ".jpeg"), plot = last_plot(),width = 11.29, height = 7.02, units = "cm")
}


#### Generacion de Graficas por Tipo de Delito y Distancia.

for(i in TLocalidades)
{for (j in TDelitos) {
  funcion(i, j)
}}

